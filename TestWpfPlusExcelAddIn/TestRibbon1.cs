﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Interop;
using Microsoft.Office.Tools.Ribbon;
using Excel = Microsoft.Office.Interop.Excel;

namespace TestWpfPlusExcelAddIn
{
    public partial class TestRibbon1
    {
        private Excel.Application m_ExcelApplication;
        private void TestRibbon1_Load(object sender, RibbonUIEventArgs e)
        {
            try
            {
                m_ExcelApplication = Globals.ThisAddIn.Application;
            }
            catch
            {
            }
        }
        // https://stackoverflow.com/questions/56820763/vsto-wpf-modal-dialogs-cursor-doesnt-blink-in-textbox
        bool isFirstTime = true;
        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            if (isFirstTime)
            {
                // For some reason the Control.Focus() is unreliable for the first WPF window ever shown within the VSTO addin. :( 
                // So make a dummy one once before we open the real window...
                // NOTE: The reason why I never noticed such a problem before in my own project, is since my project always showed a short loading window that closed itself
                // before the main window is shown. So I could use Control.Focus() in the main window without issues
                
                // So where just make a window that is effectively hidden and closes itself as soon as it is loaded to allow the code to proceed after ShowDialog()
                var pretendLoadWindow = new Window();
                pretendLoadWindow.SizeToContent = SizeToContent.WidthAndHeight;
                pretendLoadWindow.Visibility = Visibility.Collapsed;
                pretendLoadWindow.Loaded += (_sender, _e) => pretendLoadWindow.Close();
                pretendLoadWindow.ShowDialog();
                isFirstTime = false;
            }
            var window = new Window();
            var excelHwnd = m_ExcelApplication != null ? new IntPtr(m_ExcelApplication.Hwnd) : Process.GetCurrentProcess().MainWindowHandle;
            WindowInteropHelper interopHelper = new WindowInteropHelper(window)
            {
                Owner = excelHwnd
            };
            window.Content = new UserControl1();
            window.SizeToContent = SizeToContent.WidthAndHeight;
            // FYI: just in case you have any breakpoints that switch the focus away from the Excel (to your VS IDE), then when this window is shown it won't typically get focus. Below should fix this...
            window.Loaded += (_sender, _e) => window.Focus();
            window.ShowDialog();
        }
    }
}
